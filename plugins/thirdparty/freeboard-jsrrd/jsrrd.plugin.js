(function()
{
	var jsRrdWidgetPlugin = function(settings)
	{
		var self = this;
		var currentSettings = settings;
                var graph_id = "mygraph-" + Math.random().toString(16).slice(2);

		var myTextElement = $('<div id="' + graph_id  + '"></div>');

		createGraph(currentSettings.rrd_url);

                function createGraph(rrdUrl){
                 myTextElement.html('<script type="text/javascript"> \
                 var graph_opts={legend: { noColumns:4}}; \
                 var ds_graph_opts={"Oscilator":{ color: "#ff8000", \
                                    lines: { show: true, fill: true, fillColor:"#ffff80" } }, \
                                    "Idle":{ label: "IdleJobs", color: "#00c0c0", \
                                    lines: { show: true, fill: true} }, \
                                    "Running":{color: "#000000",yaxis:2}}; \
                flot_obj=new rrdFlotAsync("' + graph_id + '", \
               "' + rrdUrl + '", null, graph_opts,ds_graph_opts);');
                }

		self.render = function(containerElement)
		{
			$(containerElement).append(myTextElement);
		}
		
		self.getHeight = function()
		{
                    return 8;
		}
		
		self.onSettingsChanged = function(newSettings)
		{
			currentSettings = newSettings;
                        createGraph(currentSettings.rrd_url);
		}
		
		self.onCalculatedValueChanged = function(settingName, newValue)
		{
		}

		self.onDispose = function()
		{
		}
	}

    freeboard.loadWidgetPlugin({
                type_name   : "jsrrd_plugin",
                display_name: "JsRRD",
                description : "Show a graph from a RRD hosted on a webserver.",
                external_scripts: [
                        "./plugins/thirdparty/freeboard-jsrrd/javascriptrrd-1.1.1/src/lib/binaryXHR.js",
                        "./plugins/thirdparty/freeboard-jsrrd/javascriptrrd-1.1.1/src/lib/rrdFile.js",
                        "./plugins/thirdparty/freeboard-jsrrd/javascriptrrd-1.1.1/src/lib/rrdFilter.js",
                        "./plugins/thirdparty/freeboard-jsrrd/javascriptrrd-1.1.1/src/lib/rrdFlotAsync.js",
                        "./plugins/thirdparty/freeboard-jsrrd/javascriptrrd-1.1.1/src/lib/rrdFlot.js",
                        "./plugins/thirdparty/freeboard-jsrrd/javascriptrrd-1.1.1/src/lib/rrdFlotMatrix.js",
                        "./plugins/thirdparty/freeboard-jsrrd/javascriptrrd-1.1.1/src/lib/rrdFlotSupport.js",
                        "./plugins/thirdparty/freeboard-jsrrd/javascriptrrd-1.1.1/src/lib/rrdMultiFile.js",
                        "./plugins/thirdparty/freeboard-jsrrd/javascriptrrd-1.1.1/src/lib/javascriptrrd.js",
                        "./plugins/thirdparty/freeboard-jsrrd/javascriptrrd-1.1.1/flot/excanvas.min.js",
                        "./plugins/thirdparty/freeboard-jsrrd/javascriptrrd-1.1.1/flot/jquery.colorhelpers.min.js",
                        "./plugins/thirdparty/freeboard-jsrrd/javascriptrrd-1.1.1/flot/jquery.flot.crosshair.min.js",
                        "./plugins/thirdparty/freeboard-jsrrd/javascriptrrd-1.1.1/flot/jquery.flot.fillbetween.min.js",
                        "./plugins/thirdparty/freeboard-jsrrd/javascriptrrd-1.1.1/flot/jquery.flot.image.min.js",
                        "./plugins/thirdparty/freeboard-jsrrd/javascriptrrd-1.1.1/flot/jquery.flot.min.js",
                        "./plugins/thirdparty/freeboard-jsrrd/javascriptrrd-1.1.1/flot/jquery.flot.navigate.min.js",
                        "./plugins/thirdparty/freeboard-jsrrd/javascriptrrd-1.1.1/flot/jquery.flot.pie.min.js",
                        "./plugins/thirdparty/freeboard-jsrrd/javascriptrrd-1.1.1/flot/jquery.flot.resize.min.js",
                        "./plugins/thirdparty/freeboard-jsrrd/javascriptrrd-1.1.1/flot/jquery.flot.selection.min.js",
                        "./plugins/thirdparty/freeboard-jsrrd/javascriptrrd-1.1.1/flot/jquery.flot.stack.min.js",
                        "./plugins/thirdparty/freeboard-jsrrd/javascriptrrd-1.1.1/flot/jquery.flot.symbol.min.js",
                        "./plugins/thirdparty/freeboard-jsrrd/javascriptrrd-1.1.1/flot/jquery.flot.threshold.min.js",
                        "./plugins/thirdparty/freeboard-jsrrd/javascriptrrd-1.1.1/flot/jquery.flot.time.min.js",
                        "./plugins/thirdparty/freeboard-jsrrd/javascriptrrd-1.1.1/flot/jquery.flot.tooltip.min.js"

                ],
                fill_size : true,
                settings    : [
            {
                name        : "rrd_url",
                display_name: "RRD URL",
                type        : "text",
                required    : true
            }
        ],

        newInstance   : function(settings, newInstanceCallback)
        {
            newInstanceCallback(new jsRrdWidgetPlugin(settings));
        }
    });
}());
